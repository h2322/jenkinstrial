const express = require ("express")
const routeremp= require("./empservice")
const cors= require ("cors")
const app= express()
app.use(cors('*'))
app.use(express.json())
app.use('/get',routeremp)

app.listen(4000,'0.0.0.0',()=>{
    console.log("your app is listening on port 4000")
})